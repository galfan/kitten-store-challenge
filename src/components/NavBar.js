import { Menu } from 'antd';
import React from 'react'
import {
    Link,
  } from "react-router-dom";
import { useStateContext } from '../state-management/index'
import { observer } from 'mobx-react'
const NavBar = observer(function(props) {
    const { title, rootPath, setRootPath } = useStateContext();
    if(document.location.pathname !== '/') setRootPath(false)
    return (
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          style={{ lineHeight: '64px' }}
        >
            {
                (!rootPath || document.location.pathname !== '/') && 
                <Menu.Item key={1}><Link to="/" onClick={() => setRootPath(true)}>Go Back To Home</Link></Menu.Item>
            }
            <Menu.Item key={2}>{ title }</Menu.Item>
        </Menu>)
})
export default NavBar 
  