import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";

import { StateContextProvider } from './state-management/index'
import NavBar from './components/NavBar'
import './styles/App.css';
import KittensCatalog from './views/kittens-catalog'
import BreedDetails from './views/kitten-breed-details'
import Orders from './views/orders'
import { Layout, Typography } from 'antd';
const { Header, Footer, Content } = Layout;
const { Title } = Typography;


function App() {

  return (
    <Router>
      <div className="App">
        <MainLayout>
          <NavBar/>
          <Switch>
            <Route path='/breeds/:breedsID/details'>
              <BreedDetails/>
            </Route>
            <Route path="/orders/:breedsID">
              <Orders/>
            </Route>
            <Route path="/">
              <KittensCatalog/>
            </Route>
          </Switch>
        </MainLayout>
      </div>
    </Router>
  );
}
const MainLayout = function(props) {
  return (
    <div>
      <StateContextProvider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0, marginTop: '20px' }}><Title>Kitten Store</Title></Header>
          <Content>{props.children}</Content>
          <Footer></Footer>
        </Layout>
      </StateContextProvider>
    </div>
  )
}



export default App;
