import React from 'react';
import { Typography  } from 'antd';
import {
    useParams,
  } from "react-router-dom";
import { Table } from 'antd';
const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'LastName',
      dataIndex: 'lastname',
      key: 'lastname',
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
        title: 'Gender',
        dataIndex: 'gender',
        key: 'gender',
    },
    {
        title: 'Shipping Address',
        dataIndex: 'shippingAddress',
        key: 'shippingAddress',
    },
    {
        title: 'Phone',
        dataIndex: 'phone',
        key: 'phone',
    },
    {
        title: 'Shipping Date',
        dataIndex: 'shippingDate',
        key: 'shippingDate',
    },
  ];
const { Title } = Typography
function BreedDetails() {
    let { breedsID } = useParams();
    let orders = JSON.parse(localStorage.getItem('orders'));

    return <div style={{marginTop: '1rem', padding: '.5rem'}}>
    {
        orders && orders.filter(item => item.breedsID === breedsID).length ? <Table columns={columns} dataSource={orders.filter(item => item.breedsID === breedsID)} />
        : <Title>No Orders!</Title>
    }
  </div>;
  }

export default BreedDetails