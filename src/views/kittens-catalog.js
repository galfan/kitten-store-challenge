import React from 'react'
import { Link } from "react-router-dom";
  
import { useObserver } from 'mobx-react' // 6.x
import { Card, Icon, Col, Row, Button  } from 'antd';
import { useStateContext } from '../state-management/index'

const { Meta } = Card;


const KittensCatalog = function() {
    const { setRootPath, setTitle, dataSource, incrementLove } = useStateContext();
    const dispatch = (item, text) => {
        setTitle(item.name + text);
        setRootPath(false)
    }
    return useObserver(() => (
        <Row gutter={16}>
        {
            dataSource.map((item, key) => (
                <Col span={8} key={key}>
                    <Card
                        hoverable
                        style={{ width: 240, marginTop: '1rem', marginLeft: '1rem' }}
                        actions={[
                            <Link to={'breeds/' + key + '/details'} onClick={() => dispatch({name: 'Breeds'}, ' and Orders')}><Icon type="eye" key="eye"/></Link>,
                            <Icon type="heart" key="heart" onClick={() => incrementLove(key)}/>,
                        ]}
                        cover={<img style={{maxHeight: 150}} alt={item.name} src={item.imgSource}/>}
                    >
                        <Meta
                        title={item.name}
                        description={item.price}
                        />
                        <p>Love Given: {item.loveGiven}</p>
                        <Button type="primary"><Link to={"/orders/" + key} onClick={() => dispatch(item, ' Orders')}>Show Orders</Link></Button>
                    </Card>
                </Col>
            ))
        }
        </Row>
    ))
}

export default KittensCatalog