import React from 'react';
import { Row, Col, Typography, Alert, Divider  } from 'antd';
import { useFormik } from 'formik';
import {
    useParams,
  } from "react-router-dom";
  const {Paragraph} = Typography
function BreedDetails() {

    let { breedsID } = useParams();
    let kittensBreed = JSON.parse(localStorage.getItem('kittensBreed')) || [];
    let breedInformation = kittensBreed[breedsID];

    return <Row>
            { breedInformation &&
                <Col span={12}>
                    <Row>
                        <Col span={24}>
                            <Divider orientation="left" style={{ color: '#333', fontWeight: 'normal' }}>
                                <b>Breed Information</b>
                            </Divider>
                        </Col>
                        <Col span={24}>
                        <Row gutter={[24, 24]}>
                            <Col span={12}>
                                <Paragraph strong> Name: </Paragraph> <Paragraph>{breedInformation.name} </Paragraph> 
                            </Col>
                            <Col span={12}>
                                <Paragraph strong> Price: </Paragraph> <Paragraph>{breedInformation.price}</Paragraph> 
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                <Paragraph strong> Description: </Paragraph> <Paragraph>{breedInformation.description}  </Paragraph>
                            </Col>
                            <Col span={12}>
                                <Paragraph strong>Love Given: </Paragraph> <Paragraph>{breedInformation.loveGiven} </Paragraph> 
                            </Col>
                        </Row> 
                    </Col>
                    </Row>
                </Col>
            }
                <Col span={12} style={{backgroundColor: '#00152929'}}>
                    <Row justify="space-around" align="middle">
                        <Col span={24}>
                            <Divider orientation="left" style={{ color: '#333', fontWeight: 'normal' }}>
                                <strong>Add New Order</strong>
                            </Divider> 
                        </Col>
                        <Col span={24}>
                            <Form/>
                        </Col>
                    </Row>
                </Col>
        </Row>
  }

const validate = values => {
  const errors = {};
  if (!values.firstname) {
    errors.firstname = 'Required';
  } 

  if (!values.lastname) {
    errors.lastname = 'Required';
  } else if (values.lastname.length > 20) {
    errors.lastname = 'Must be 20 characters or less';
  }

  if (!+values.age) {
    errors.age = 'Must Be Numbers';
  } 
  if (!+values.phone) {
    errors.phone = 'Must Be Numbers';
  } 

  return errors;
};
  const Form =() => {
    let { breedsID } = useParams();
    const [isActive,setIsActive] = React.useState(false);
    const formik = useFormik({
        initialValues: {
          firstname: '',
          lastname: '',
          age: 0,
          gender: '',
          shippingAddress: '',
          phone: '',
          shippingDate: '',
        },
        validate,
        onSubmit: (values, { resetForm, setStatus }) => {
            localStorage.orders =  JSON.stringify([ ...(localStorage.orders ? JSON.parse(localStorage.orders) : []), {...values, breedsID } ])
            setIsActive(true);
            resetForm({})
            setStatus({success: true})
            setTimeout(() => {setIsActive(false);}, 1000)
        },
      });
      return (
          <form onSubmit={formik.handleSubmit}>
            <Row gutter={[24, 24]}>
                <Col span={12}>
                    <label style={{marginRight: '.8rem'}} htmlFor="firstname">First Name</label>
                    <input
                        id="firstname"
                        name="firstname"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.firstname}
                        />
                    {formik.errors.firstname ? <Paragraph type="warning">{formik.errors.firstname}</Paragraph> : null}
                </Col>
                <Col span={12}>
                    <label style={{marginRight: '.8rem'}} htmlFor="lastname">Last Name</label>
                    <input
                        id="lastname"
                        name="lastname"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.lastname}
                        />
                    {formik.errors.lastname ? <Paragraph type="warning">{formik.errors.lastname}</Paragraph> : null}
                </Col>
            </Row>
            <Row gutter={[24, 24]}>
                <Col span={12}>
                    <label style={{marginRight: '.8rem'}} htmlFor="age">Age</label>
                    <input
                        id="age"
                        name="age"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.age}
                        />
                    {formik.errors.age ? <Paragraph type="warning">{formik.errors.age}</Paragraph> : null}
                </Col>
                <Col span={12}>
                    <label style={{marginRight: '.8rem'}} htmlFor="gender">Gender</label>
                    <input
                        id="gender"
                        name="gender"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.gender}
                        />
                    {formik.errors.gender ? <Paragraph type="warning">{formik.errors.gender}</Paragraph> : null}
                </Col>
            </Row>
                    { isActive && <Alert message="Recorded!" type="success" /> }
            <Row gutter={[24, 24]}>
                <Col span={12}>
                    <label style={{marginRight: '.8rem'}} htmlFor="shippingAddress">Shipping Address</label>
                    <input
                        id="shippingAddress"
                        name="shippingAddress"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.shippingAddress}
                    />
                    {formik.errors.shippingAddress ? <Paragraph type="warning">{formik.errors.shippingAddress}</Paragraph> : null}
                </Col>
                <Col span={12}>
                    <label style={{marginRight: '.8rem'}} htmlFor="phone">Phone</label>
                    <input
                        id="phone"
                        name="phone"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.phone}
                    />
                    {formik.errors.phone ? <Paragraph type="warning">{formik.errors.phone}</Paragraph> : null}
                </Col>
            </Row>
            <Row gutter={[24, 24]}>
                <Col span={24}>
                    <label style={{marginRight: '.8rem'}} htmlFor="shippingDate">Shipping Date</label>
                    <input
                        id="shippingDate"
                        name="shippingDate"
                        type="date"
                        onChange={formik.handleChange}
                        value={formik.values.shippingDate}
                    />
                    {formik.errors.shippingDate ? <Paragraph type="warning">{formik.errors.shippingDate}</Paragraph> : null}
                </Col>
            </Row>
            <button type="submit" style={{marginBottom: '1rem'}}>Submit</button>
        </form>
      )
  }
export default BreedDetails