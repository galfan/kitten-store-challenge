import { useLocalStore } from 'mobx-react'
import kittensBreed from '../mockData/kittens-breed.js'

import React from 'react';
const AppState = () => {
  return {
    title: "Catalog",
    rootPath: true,
    kittensBreed: localStorage.kittensBreed ? JSON.parse(localStorage.getItem('kittensBreed')) : kittensBreed,
    get dataSource() { return this.kittensBreed }, 
    // changeStatus(status, key) {
    //     localStore.kittensBreed[key][status] = !localStore.kittensBreed[key][status]
    //     localStore.setInLocalStorage()
    // },
    setInLocalStorage() {
        localStorage.kittensBreed = JSON.stringify(this.kittensBreed);
    },
    incrementLove(key) {
        this.kittensBreed[key].loveGiven++;
        this.setInLocalStorage()
    },
    setRootPath(newPath) {
      this.rootPath = newPath;
    },
    setTitle(newValue) {
      this.title = newValue;
    }
  }
}

const StateContext = React.createContext();
export const StateContextProvider = ({children}) => {
  const store = useLocalStore(AppState);
  return <StateContext.Provider value={store}
  >
    { children }
  </StateContext.Provider>
}

export const useStateContext= () => {
  const context = React.useContext(StateContext);
  if (context === undefined) throw new Error('useStore must be used within a StoreProvider.');
  return context
}

