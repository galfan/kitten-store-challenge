# Create A Kitten store Catalog. Use a JSON to pull the data for the challenge


## Kittens catalog sections /Catalog
--------

The catalog should be a list of kitten breed Cards, Each card should contain the kitten breed image example, name, cost, a view icon, a love icon.

A User should be able to open the kitten breed details when clicking to the view Icon.

A user should be able to give love to the kitten card, and a counter of how many times the user has given love to the kitten should be displayed and persisted between sessions.

## Breed Details Page /breeds/:breedsID/details
---

A User should be able to see on the left side, more details about the kitten breed and on the right side, a form in order to send a buy order.

The form should contain fields for the buyers:

- name

- last name

- age

- gender

- shipping address

- phone.

- shipping date


## Order List Page /orders

A user should be able to see the orders when he can see the breeds image, and general description, and the order information.


### Stack to be used,


- Mobx

- Formick

- React hooks

- React Context

- Antd Design